#ifndef DIGI_H
#define DIGI_H

#define MAX_BUFFER_SIZE 1024

#include <stdbool.h>
#include <stddef.h>

bool read_int(int *dest);
bool read_str(char *dest, size_t dest_size, const char *prompt);

#endif //DIGI_H
