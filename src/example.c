#include <stdio.h>

#include "digi.h"

int main() {
    char small_text[8];
    char large_text[40];
    bool success = false;
    int num = 0;

    if (read_str(small_text, sizeof(small_text), "in few words: ")) {
        printf("yep, less than 8 characters!\n");
    }

    do {
        printf("or a little more words: ");
        success = read_str(large_text, sizeof(large_text), NULL);

        if (!success) {
            printf("nop, please try again\n");
        }

    } while (!success);

    do {
        printf("1 + 1 = ");
    } while (!(read_int(&num) && num == 2));
    
    return 0;
}
