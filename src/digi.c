#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "digi.h"

bool read_int(int *dest) {
    char buffer[MAX_BUFFER_SIZE];
    char *end_ptr;

    if (fgets(buffer, sizeof(buffer), stdin) != NULL) {
        const int value = strtol(buffer, &end_ptr, 10);

        if (buffer[0] != '\n' && (*end_ptr == '\n' || *end_ptr == '\0')) {
            *dest = value;
            return true;
        }
    }
    return false;
}

bool read_str(char *dest, const size_t dest_size, const char *prompt) {
    char buffer[MAX_BUFFER_SIZE];

    if (prompt != NULL) {
        printf("%s", prompt);
    }

    fgets(buffer, sizeof(buffer), stdin);

    if (strlen(buffer) > dest_size) {
        fprintf(stderr, "Your input exceeds the max lentgh.\n");
        return false;
    }

    strncpy(dest, buffer, dest_size - 1);
    dest[dest_size - 1] = '\0';

    return true;
}
