# reference: https://stackoverflow.com/a/30602701/378589

SRC_DIR := src
OBJ_DIR := obj
LIB_DIR := lib
BIN_DIR := bin

EXE := $(BIN_DIR)/example

LIB := $(LIB_DIR)/libdigi
OUT_FILE_NAME = $(LIB).a

SRC := $(wildcard $(SRC_DIR)/*.c)
OBJ := $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SRC))

CC ?= gcc
CPPFLAGS := -Iinclude -MMD -MP
CFLAGS := -g -Wall -Wextra -pedantic -std=c11

.PHONY: all clean run

all: $(OUT_FILE_NAME)

$(OUT_FILE_NAME): $(OBJ) | $(LIB_DIR)
	ar rcs $@ $^ 

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(LDFLAGS) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(OBJ_DIR) $(LIB_DIR) $(BIN_DIR):
	mkdir -p $@

$(EXE): $(OBJ) | $(BIN_DIR)
	$(CC) $^ -o $@

run: $(EXE)
	@./$(EXE)

clean:
	@$(RM) -rv $(BIN_DIR) $(OBJ_DIR) $(LIB_DIR)

-include $(OBJ:.o=.d)
